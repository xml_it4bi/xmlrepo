package xml.owlparser.parser;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import xml.pddlparser.model.OwlsInput;
import xml.pddlparser.model.OwlsOutput;
import xml.pddlparser.model.OwlsPrecondition;
import xml.pddlparser.model.OwlsResult;
import xml.pddlparser.model.OwlsService;

/**
 * {@link OwlsParser} is used for parsing file content into {@link OwlsService} entity
 *
 */
public class OwlsParser {

	private static String SERVICE_SERVICE_TAG = "service:Service";
	private static String SERVICE_PRESENTS_TAG  = "service:presents";
	private static String SERVICE_DESCRIBEBY_TAG  = "service:describedBy";
	private static String RDF_RESOURCE_ATTRIBUTE = "rdf:resource";

	/**
	 * Gets the necessary information from the OWL-S file. 
	 * It returns a list of OWL-S document with the Inputs, Outputs, Preconditions and Result 
	 * @param filePath
	 * @return list of {@link OwlsService}
	 */
	public List<OwlsService> parse(String filePath) {
		List<OwlsService> serviceList = new ArrayList<>();
		try {
			// Get the file to parse
			File inputFile = new File(filePath);
			// Get the DOM Builder
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();

			// Load and parse XML
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(inputFile);

			// get root node
			doc.getDocumentElement().normalize();
			
			// from root node, find all service node
			// then loop through each service node to parse and get information
			// return PddlAction for each service node
			NodeList serviceNodeList = doc.getElementsByTagName(SERVICE_SERVICE_TAG);
			for(int i=0; i<serviceNodeList.getLength(); i++){
				OwlsService service = parseService(doc, serviceNodeList.item(i));
				serviceList.add(service);
			}

		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		return serviceList;
	}
	
	/**
	 * Process each service node to get {@link OwlsService}
	 * <p>
	 * 	<b>Step</b> <br/>
	 * 1. Process service node to get information about service profile, service process. <br/>
	 * 2. From profileId, find profile element node (tag <profile:Profile>). Get service name from profile (tag <profile:ServiceName). <br/>
	 * 		General idea: Because in <profile:Profile>, we can get IDREF of input, output, parameter, precondition, result. <br/>
	 * 2.1 From profile element, get names of all parameters.<br/>
	 * 		2.2 From profile element, get ids of all inputs, outputs, preconditions, result ids. <br/>
	 * 	3. Now, re-check if there is any entities that we can not get from <profile:Profile>. Try getting it from process. <br/>
	 * 		3.1 From processId, find process element node. <br/>
	 * 		3.2 Getting ids from process element <br/>
	 * </p>
	 * @param document
	 * @param serviceNode
	 * @return
	 * @throws XPathExpressionException
	 */
	public OwlsService parseService(Document document, Node serviceNode) throws XPathExpressionException {
		// 1. process service node to get information about service profile, service process
		OwlsService owlsService = new OwlsService();
		String serviceId = ((Element) serviceNode).getAttribute("rdf:ID");
		owlsService.setServiceId(serviceId);
		NodeList nodeList = serviceNode.getChildNodes();
		String profileId = "";
		String processId = "";
		for(int i =0; i<nodeList.getLength(); i++){
			Node node = nodeList.item(i);
			// get profile id
			if(node.getNodeType() == Node.ELEMENT_NODE && SERVICE_PRESENTS_TAG.equals(node.getNodeName())){
				profileId = ((Element) node).getAttribute(RDF_RESOURCE_ATTRIBUTE);
				// remove character "#" in IDREF
				profileId = profileId.substring(1, profileId.length());
			}
			else{
				// get process id
				if(node.getNodeType() == Node.ELEMENT_NODE && SERVICE_DESCRIBEBY_TAG.equals(node.getNodeName())){
					processId = ((Element) node).getAttribute(RDF_RESOURCE_ATTRIBUTE);
					// remove character "#" in IDREF
					processId = processId.substring(1, processId.length());
				}
			}
		}
		
		// 2. From profileId, find profile element node (tag <profile:Profile>)
		String profileXPath = "//*[local-name()='Profile'][@*[local-name()='ID' and .='" + profileId + "']]";
		NodeList profileNodeList = findElementByXpath(document, profileXPath);
		Element profileElement = (Element) profileNodeList.item(0);
		
		// Get service name from profile (tag <profile:ServiceName)
		Node serviceNameNode = profileElement.getElementsByTagName("profile:serviceName").item(0);
		owlsService.setServiceName(serviceNameNode.getTextContent().trim());
		
		// General idea: Because in <profile:Profile>, we can get IDREF of input, output, parameter, precondition, result. 
		// First, Check those entities in profile 
		// If they are not existed, get input, output, result, precondition from process
		
		// 2.1 From profile element, get names of all parameters
		List<String> parameterNames = getNodeIdList(profileElement, "profile:hasParameter");
		owlsService.setParameters(parameterNames);
		
		// 2.2 From profile element, get ids of all inputs, outputs, preconditions, result ids
		List<String> inputIds = getNodeIdList(profileElement, "profile:hasInput");
		List<String> outputIds = getNodeIdList(profileElement, "profile:hasOutput");
		List<String> preconditionIds = getNodeIdList(profileElement, "profile:hasPrecondition");
		List<String> resultIds = getNodeIdList(profileElement, "profile:hasResult");
		
		// 3. Now, re-check if there is any entities that we can not get from <profile:Profile>. Try getting it from process.
		// 3.1 From processId, find process element node
		String processXPath = "//*[@*[local-name()='ID' and .='" + processId + "']]";
		NodeList processNodeList = findElementByXpath(document, processXPath);
		Element processElement = (Element) processNodeList.item(0);
		
		// 3.2 Getting ids from process element if they are not existed in profile declaration
		if(inputIds.isEmpty()){
			inputIds = getNodeIdList(processElement, "profile:hasInput");
		}
		if(outputIds.isEmpty()){
			outputIds = getNodeIdList(processElement, "profile:hasOutput");
		}
		if(preconditionIds.isEmpty()){
			preconditionIds = getNodeIdList(processElement, "profile:hasPrecondition");
		}
		if(resultIds.isEmpty()){
			resultIds = getNodeIdList(processElement, "profile:hasResult");
		}
		
		// 3.3 With ids from above step, find node by id and parse node to get information
		// 3.3.1 Parse each input nodes to get information
		if(!inputIds.isEmpty()){			
			List<OwlsInput> inputs = parseInputs(document, inputIds, "Input");
			owlsService.setInputs(inputs);
		}
		
		// 3.3.1 Parse each output nodes to get information
		if(!outputIds.isEmpty()){
			List<OwlsOutput> outputs = parseOutputs(document, outputIds, "Output");			
			owlsService.setOutputs(outputs);
		}
		
		// 3.3.1 Parse each precondition nodes to get information
		if(!preconditionIds.isEmpty()){			
			owlsService.setPreconditions(parsePreconditions(document, preconditionIds));
		}
		
		// 3.3.1 Parse each result nodes to get information
		if(!resultIds.isEmpty()){			
			owlsService.setResults(parseResults(document, resultIds));
		}
		
		return owlsService;
	}
	
	/**
	 * Get all ids of node which have tags of nodeType under rootElement
	 * <p>
	 * <b>Step:</b> <br/>
	 * 1. From root element, get list of children by nodeType. <br/>
	 * 2. Just need to get name of each parameter <br/>
	 * </p>
	 * @param rootElement
	 * @param nodeType
	 * @return
	 */
	public List<String> getNodeIdList(Element rootElement, String nodeType){
		// 2.1 From root element, get list of children by nodeType 
		NodeList nodeList = rootElement.getElementsByTagName(nodeType);
		// 2.2 Just need to get name of each parameter
		List<String> nodeIds = new ArrayList<>();
		if(nodeList.getLength() > 0){
			for(int i =0; i<nodeList.getLength(); i++){
				Node node = nodeList.item(i);
				String idref = ((Element) node).getAttribute(RDF_RESOURCE_ATTRIBUTE);
				// remove character "#" from IDREF
				nodeIds.add(idref.substring(1, idref.length()));
			}				
		}
		return nodeIds;
	}
	
	/**
	 * In the tree of a document, get the element indicated in the XPath expression
	 * @param document - Root document
	 * @param xpathStr -XPath expression
	 * @return {@link NodeList}
	 * @throws XPathExpressionException
	 */
	public NodeList findElementByXpath(Document document, String xpathStr) throws XPathExpressionException{
		XPathFactory factory = XPathFactory.newInstance();
		XPath xpath = factory.newXPath();

		NodeList result = (NodeList) xpath.evaluate(xpathStr, document, XPathConstants.NODESET);
		return result;
	}
	
	/**
	 * In the tree of an element, get the element indicated in the XPath expression
	 * @param element - Element we start to find
	 * @param xpathStr - XPath expression
	 * @return {@link NodeList}
	 * @throws XPathExpressionException
	 */
	public NodeList findElementByXpath(Element element, String xpathStr) throws XPathExpressionException{
		XPathFactory factory = XPathFactory.newInstance();
		XPath xpath = factory.newXPath();

		NodeList result = (NodeList) xpath.evaluate(xpathStr, element, XPathConstants.NODESET);
		return result;
	}


	/**
	 * Get information of all preconditions in ids list.
	 * @param doc - Document where we start to find
	 * @param ids - List of precondition ids
	 * @return {@link OwlsPrecondition}
	 * @throws XPathExpressionException
	 */
	public List<OwlsPrecondition> parsePreconditions(Document doc, List<String> ids) /**Get preconditions from a document **/ throws XPathExpressionException {
		List<OwlsPrecondition> preconditions = new LinkedList<>();

		// Find list of SWRL-condition node by ids 
		List<Node> nList = new ArrayList<>();
		for(int i =0; i< ids.size(); i++){
			NodeList node = findElementByXpath(doc, "//*[local-name()='SWRL-Condition'][@*[local-name()='ID' and .='" + ids.get(i) + "']]");
			nList.add(node.item(0));
		}
		
		// Iterate through the nodes and extract the data
		for (int temp = 0; temp < nList.size(); temp++) {
			OwlsPrecondition precondition3 = new OwlsPrecondition();
			Node nNode = nList.get(temp);
			if (nNode.getNodeType() == Node.ELEMENT_NODE) {
				Element eElement = (Element) nNode;
				// Get the id
				if (!(eElement.getAttribute("rdf:ID") == null)) {
					precondition3.setId(eElement.getAttribute("rdf:ID"));
				}
				
				// Find propertyPredicate node
				NodeList propertyPredicateNodeList = eElement.getElementsByTagName("swrl:propertyPredicate");
				if(propertyPredicateNodeList.getLength() >0){
					Element propertyPredicateElement = (Element) propertyPredicateNodeList.item(0);
					// Get property predicate
					precondition3.setPropertyPredicate(propertyPredicateElement.getAttribute(RDF_RESOURCE_ATTRIBUTE));
					
				}
				
				// Find argument
				// Loop through child node of PropertyPredicate to get argument list
				NodeList argumentNodeList = findElementByXpath(eElement, ".//*[contains(local-name(), 'argument')]");
				List<String> argumentList = new LinkedList<>();
				for(int i=0; i< argumentNodeList.getLength(); i++){
					// Get argument
					Element argumentEle = (Element) argumentNodeList.item(i);
					argumentList.add(argumentEle.getAttribute(RDF_RESOURCE_ATTRIBUTE));
				}
				precondition3.setArguments(argumentList);
			}
			preconditions.add(precondition3);// add precondition to list
		}
		return preconditions;

	}
	
	/**
	 * Get information of all results in ids list.
	 *<p> 
	 *	<b> Step to get results from a document. </b>
	 * 1. Find list of SWRL-condition node by ids <br/>
	 * 2. Iterate through the nodes and extract the data <br/>
	 * 	2.1. Find propertyPredicate. <br/>
	 * 	2.2. Find arguments. <br/>
	 * </p>
	 * @param doc - Document where we start to find
	 * @param ids - List of result ids
	 * @return {@link OwlsResult}
	 * @throws XPathExpressionException
	 */
	public List<OwlsResult> parseResults(Document doc, List<String> ids) throws XPathExpressionException {
		List<OwlsResult> results = new LinkedList<>();

		// Find list of SWRL-condition node by ids 
		List<Node> nList = new ArrayList<>();
		for(int i =0; i< ids.size(); i++){
			NodeList node = findElementByXpath(doc, "//*[local-name()='Result'][@*[local-name()='ID' and .='" + ids.get(i) + "']]");
			nList.add(node.item(0));
		}
		
		// Iterate through the nodes and extract the data
		for (int temp = 0; temp < nList.size(); temp++) {
			OwlsResult result = new OwlsResult();
			Node nNode = nList.get(temp);
			if (nNode.getNodeType() == Node.ELEMENT_NODE) {
				Element eElement = (Element) nNode;
				// Get the id
				if (!(eElement.getAttribute("rdf:ID") == null)) {
					result.setId(eElement.getAttribute("rdf:ID"));
				}
				
				// Find propertyPredicate node
				NodeList propertyPredicateNodeList = eElement.getElementsByTagName("swrl:propertyPredicate");
				if(propertyPredicateNodeList.getLength() >0){
					Element propertyPredicateElement = (Element) propertyPredicateNodeList.item(0);
					// Get property predicate
					result.setPropertyPredicate(propertyPredicateElement.getAttribute(RDF_RESOURCE_ATTRIBUTE));
					
				}
				
				// Find argument
				// Loop through child node of PropertyPredicate to get argument list
				NodeList argumentNodeList = findElementByXpath(eElement, ".//*[contains(local-name(), 'argument')]");
				List<String> argumentList = new LinkedList<>();
				for(int i=0; i< argumentNodeList.getLength(); i++){
					// Get argument
					Element argumentEle = (Element) argumentNodeList.item(i);
					argumentList.add(argumentEle.getAttribute(RDF_RESOURCE_ATTRIBUTE));
				}
				result.setArguments(argumentList);
			}
			results.add(result);// add precondition to list
		}
		return results;

	}


	/**
	 * From an OWL-S Document get attributes, inputs, datatypes and add it to a list of {@link OwlsInput}
	 * <p>
	 * <b>Step to Get inputs from a document </b>
	 *  1. Get list of input node. <br/>
	 *  2.Iterate through the nodes and extract the data <br/>
	 * </p>
	 * @param doc - Document where we start to find
	 * @param ids - List of input ids
	 * @return {@link OwlsInput}
	 * @throws XPathExpressionException
	 */
	public List<OwlsInput> parseInputs(Document doc, List<String> ids, String type) throws XPathExpressionException{
		List<OwlsInput> inputs = new LinkedList<>();

		// Get list of input node
		List<Node> nList = new ArrayList<>();
		for(int i =0; i< ids.size(); i++){
			NodeList node = findElementByXpath(doc, "//*[local-name()='" +  type +  "'][@*[local-name()='ID' and .='" + ids.get(i) + "']]");
			nList.add(node.item(0));
		}

		// Iterate through the nodes and extract the data
		for (int temp = 0; temp < nList.size(); temp++) {
			OwlsInput input = new OwlsInput();
			Node nNode = nList.get(temp);
			if (nNode.getNodeType() == Node.ELEMENT_NODE) {
				Element eElement = (Element) nNode;
				// Get the Input ID and put it in OwlsInput

				if (eElement.getAttribute("rdf:ID") != null) {
					input.setId(eElement.getAttribute("rdf:ID"));
				}

				// Get Label ID and put in OwlsInput

				if (eElement.getElementsByTagName("rdfs:label").item(0).getTextContent() != null) {
					input.setLabel(eElement.getElementsByTagName("rdfs:label").item(0).getTextContent());
				}

				// Get the parameter datatype
				NodeList nList2 = eElement.getElementsByTagName("process:parameterType");
				Node nNode2 = nList2.item(0); // 0 to get always the same
				Element eElement2 = (Element) nNode2;

				// Get the parameter datatype and put it in OwlsInput
				input.setDataType(eElement2.getTextContent());
				
				// Add to linked list
				inputs.add(input);
			}

		}
		return inputs;
	}


	/**
	 * Get information of all outputs in ids list.
	 * <p>
	 * <b>Step:</b> <br/>
	 * 1. Get list of output node. <br/>
	 * 2.Iterate through the nodes and extract the data <br/>
	 * </p>
	 * @param doc - Document where we start to find
	 * @param ids - List of output ids
	 * @return {@link OwlsOutput}
	 * @throws XPathExpressionException
	 */
	public List<OwlsOutput> parseOutputs(Document doc, List<String> ids, String type) throws XPathExpressionException{
		List<OwlsOutput> outputs = new LinkedList<>();

		// Get list of output node
		List<Node> nList = new ArrayList<>();
		for(int i =0; i< ids.size(); i++){
			NodeList node = findElementByXpath(doc, "//*[local-name()='" +  type +  "'][@*[local-name()='ID' and .='" + ids.get(i) + "']]");
			nList.add(node.item(0));
		}

		// Iterate through the nodes and extract the data
		for (int temp = 0; temp < nList.size(); temp++) {
			OwlsOutput output = new OwlsOutput();
			Node nNode = nList.get(temp);
			if (nNode.getNodeType() == Node.ELEMENT_NODE) {
				Element eElement = (Element) nNode;
				// Get the Output ID and put it in OwlsOutput
				if (eElement.getAttribute("rdf:ID") != null) {
					output.setId(eElement.getAttribute("rdf:ID"));
				}

				// Get Label ID and put in OwlsOutput
				if (eElement.getElementsByTagName("rdfs:label").item(0).getTextContent() != null) {
					output.setLabel(eElement.getElementsByTagName("rdfs:label").item(0).getTextContent());
				}

				// Get the parameter datatype
				NodeList nList2 = eElement.getElementsByTagName("process:parameterType");
				Node nNode2 = nList2.item(0); // 0 to get always the same
				Element eElement2 = (Element) nNode2;

				// Get the parameter datatype and put it in OwlsInput
				output.setDataType(eElement2.getTextContent());
				
				// Add to linked list
				outputs.add(output);
			}

		}
		
		return outputs;

	}
	
}
