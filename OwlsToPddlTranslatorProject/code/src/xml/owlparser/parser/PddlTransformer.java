package xml.owlparser.parser;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import xml.pddlparser.model.OwlsInput;
import xml.pddlparser.model.OwlsOutput;
import xml.pddlparser.model.OwlsPrecondition;
import xml.pddlparser.model.OwlsResult;
import xml.pddlparser.model.OwlsService;
import xml.pddlparser.model.PddlAction;
import xml.pddlparser.model.PddlEffect;
import xml.pddlparser.model.PddlPrecondition;
import xml.pddlparser.model.PddlSwrlExpression;

/**
 * {@link PddlTransformer} is used for transforming from {@link OwlsService} entity into {@link PddlAction} entity 
 *
 */
public class PddlTransformer {

	/**
	 * Transfer from {@link OwlsService} entity to {@link PddlAction} entity
	 * @param {@link OwlsService} owlsService
	 * @return {@link PddlAction}
	 */
	public PddlAction transferfromOwlsToPddl(OwlsService owlsService){
		PddlAction pddlAction = new PddlAction();
		pddlAction.setActionName(owlsService.getServiceId());
		List<String> parameterSet = new LinkedList<>();
		parameterSet.addAll(owlsService.getParameters());
		
		//process input of OWL-S then transfer into preconditions of PddlDocument
		List<PddlPrecondition> preconditions = new LinkedList<>();
		for (OwlsInput owlsInput : owlsService.getInputs()) {
			PddlPrecondition precondition = new PddlPrecondition();
			precondition.setName(owlsInput.getId());
			precondition.setDatatype(removeHostPrefix(owlsInput.getDataType()));
			preconditions.add(precondition);
			parameterSet.add(precondition.getName());
		}		
		pddlAction.setPreconditions(preconditions);
		
		//process precondition of OWL-S then transfer into owlsPreconditions of PddlDocument
		List<PddlSwrlExpression> owlsConvertedPreconditions = new LinkedList<>();
		for (OwlsPrecondition owlsPrecondition : owlsService.getPreconditions()) {
			PddlSwrlExpression convertedPrecondition = new PddlSwrlExpression();
			convertedPrecondition.setId(owlsPrecondition.getId());
			convertedPrecondition.setPropertyPredicate(owlsPrecondition.getPropertyPredicate());
			convertedPrecondition.setArguments(owlsPrecondition.getArguments());
			owlsConvertedPreconditions.add(convertedPrecondition);
		}		
		pddlAction.setOwlsPrecondtions(owlsConvertedPreconditions);
		
		//process precondition of OWL-S then transfer into owlsResult of PddlDocument
		List<PddlSwrlExpression> owlsConvertedResult = new LinkedList<>();
		for (OwlsResult owlsResult : owlsService.getResults()) {
			PddlSwrlExpression convertedResult = new PddlSwrlExpression();
			convertedResult.setId(owlsResult.getId());
			convertedResult.setArguments(owlsResult.getArguments());
			convertedResult.setPropertyPredicate(owlsResult.getPropertyPredicate());
			owlsConvertedResult.add(convertedResult);
		}		
		pddlAction.setOwlsResults(owlsConvertedResult);
		
		//process output of OWL-S then transfer into preconditions of PddlDocument
		List<PddlEffect> effects = new LinkedList<>();
		for (OwlsOutput owlsOutput : owlsService.getOutputs()) {
			PddlEffect effect = new PddlEffect();
			effect.setName(owlsOutput.getId());
			effect.setDatatype(removeHostPrefix(owlsOutput.getDataType()));
			effects.add(effect);
			parameterSet.add(effect.getName());
		}
		pddlAction.setEffects(effects);
		if(parameterSet.size() >0){
			Iterator<String> parameterIter = parameterSet.iterator();
			while(parameterIter.hasNext()){
				pddlAction.getParameters().add(parameterIter.next());
			}
		}
		return pddlAction;
	}
	
	/**
	 * Remove host prefix from url
	 * <p>
	 * <b>Example:</b> from <i>"http://127.0.0.1/ontology/modified_books.owl#Paper-Back"</i> to <i>"ontology/modified_books.owl#Paper-Back"</i>
	 * </p> 
	 * @param url
	 * @return {@link String}
	 */
	public static String removeHostPrefix(String url){
		//check if url contains http or https
		if(url.contains("http://") || url.contains("https://")){
			//if yes, remove the domain part
			String [] splitUrl = url.split("/");
			if(splitUrl.length >3){
				String domainPart = splitUrl[0] + "/" + splitUrl[1] + "/" + splitUrl[3] + "/";
				String result = url.substring(domainPart.length() +1, url.length());
				return result;
			}
		}
		return url;
		
	}
}
