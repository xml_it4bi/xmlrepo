package xml.owlparser.parser;


import java.util.List;

import xml.pddlparser.model.OwlsInput;
import xml.pddlparser.model.OwlsOutput;
import xml.pddlparser.model.OwlsService;
import xml.wordnet.searcher.WNSearcher;

public class OwlsUtil {
	
	private static double RELATION_THRESHOLD = 0.5;
	
	public OwlsUtil(){
		
	}
	
	/**
	 * This method is used to compare between services.
	 * @param owls1
	 * @param owls2
	 * @return 0 - total different <br/>
	 * 			1 - service 1 is substitution of service 2 <br/>
	 * 			2 - service 1 is composition of service 2 ( output of 1 can be put into input of 2)
	 * 			3 - service 2 is composition of service 1 ( output of 2 can be put into input of 1)
	 */
	public static Integer compareService(OwlsService owls1, OwlsService owls2){
		// get input score
		double inputTotalScore = calculateTotalScoreOfInputList(owls1.getInputs(), owls2.getInputs());	
		// get input score
		double outputTotalScore = calculateTotalScoreOfOutputList(owls1.getOutputs(), owls2.getOutputs());
		// calculate average score
		double generalScore = 0;
		int inputSize1 = owls1.getInputs().size();
		int outputSize1 = owls1.getOutputs().size();
		int inputSize2 = owls2.getInputs().size();
		int outputSize2 = owls2.getOutputs().size();
		if(inputSize1 + outputSize1 + inputSize2 + outputSize2 != 0){
			generalScore = (inputTotalScore + outputTotalScore)/(inputSize1*inputSize2 + outputSize1*outputSize2);
		}
		
		// get i/o score pairwise
		// a. input 1 - output 2
		// b. input 2 - output 1
		double ioscore12 = calculateAverageScoreOfIOList(owls1.getInputs(), owls2.getOutputs());
		double ioscore21 = calculateAverageScoreOfIOList(owls2.getInputs(), owls1.getOutputs());
		
		if(generalScore >= ioscore12){
			if(generalScore >= ioscore21){
				return generalScore >= RELATION_THRESHOLD? 1: 0; 
			}
			else{
				if(ioscore21 >= ioscore12){
					return ioscore21 >= RELATION_THRESHOLD? 3: 0;
				}
				else{
					return ioscore12 >= RELATION_THRESHOLD? 2: 0;
				}
			}
		}
		else{
			if(ioscore21 >= ioscore12){
				return ioscore21 >= RELATION_THRESHOLD? 3: 0;
			}
			else{
				return ioscore12 >= RELATION_THRESHOLD? 2: 0;
			}
		}
		
	}
	
	/**
	 * This method is used for calculating total score of 2 input lists
	 * @param inputList1
	 * @param inputList2
	 * @return
	 */
	public static double calculateTotalScoreOfInputList(List<OwlsInput> inputList1, List<OwlsInput> inputList2){
		double scoreRes = 0;
		for(int i =0; i< inputList1.size(); i++){
			for(int j =0; j< inputList2.size(); j++){
				double score = WNSearcher.compute(inputList1.get(i).getId(), inputList2.get(j).getId());
				scoreRes =  scoreRes +  score;
			}
		}
		return scoreRes;
	}
	
	/**
	 * This method is used for calculating total score of 2 output lists
	 * @param outputList1
	 * @param outputList2
	 * @return
	 */
	public static double calculateTotalScoreOfOutputList(List<OwlsOutput> outputList1, List<OwlsOutput> outputList2){
		double scoreRes = 0;
		for(int i =0; i< outputList1.size(); i++){
			for(int j =0; j< outputList2.size(); j++){
				double score = WNSearcher.compute(outputList1.get(i).getId(), outputList2.get(j).getId());
				scoreRes =  scoreRes +  score;
			}
		}
		return scoreRes;
	}
	
	/**
	 * This method is used for calculating average score of 2 lists (Input and Output list)
	 * @param inputList
	 * @param outputList
	 * @return
	 */
	public static double calculateAverageScoreOfIOList(List<OwlsInput> inputList, List<OwlsOutput> outputList){
		int k =0;
		double scoreRes = 0;
		for(int i =0; i< inputList.size(); i++){
			for(int j =0; j< outputList.size(); j++){
				double score = WNSearcher.compute(inputList.get(i).getId(), outputList.get(j).getId());
				scoreRes =  scoreRes +  score;
				k++;
			}
		}
		if(k == 0){			
			return 0;
		}
		return scoreRes/k;
	}
}
