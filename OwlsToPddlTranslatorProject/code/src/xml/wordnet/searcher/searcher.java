package xml.wordnet.searcher;

/*
 * 
 */


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.jena.ext.com.google.common.base.Predicate;
import org.apache.jena.iri.impl.Main;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ResIterator;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Selector;
import org.apache.jena.rdf.model.SimpleSelector;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.util.FileManager;
import org.apache.jena.vocabulary.OWL;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.XSD;

import com.sun.xml.internal.bind.v2.model.core.ID;

import edu.cmu.lti.lexical_db.ILexicalDatabase;
import edu.cmu.lti.lexical_db.NictWordNet;
import edu.cmu.lti.ws4j.impl.WuPalmer;
import edu.cmu.lti.ws4j.util.WS4JConfiguration;
import xml.owlparser.parser.OwlsParser;
import xml.pddlparser.model.OwlsService;

// TODO: Auto-generated Javadoc
/**
 * The Class WNSearcher.
 */
public class searcher {

	/** The db. */
	private static ILexicalDatabase db = new NictWordNet();
	
	/**
	 * Compute.
	 *
	 * @param word1 the word1
	 * @param word2 the word2
	 * @return the double
	 */
	/*
	//available options of metrics
	private static RelatednessCalculator[] rcs = { new HirstStOnge(db),
			new LeacockChodorow(db), new Lesk(db), new WuPalmer(db),
			new Resnik(db), new JiangConrath(db), new Lin(db), new Path(db) };
	*/
	private static double compute(String word1, String word2) {
		WS4JConfiguration.getInstance().setMFS(true);
		double s = new WuPalmer(db).calcRelatednessOfWords(word1, word2);
		return s;
	}
	
	/**
	 * Compare.
	 *
	 * @param List of words
	 * This method compares two words and prints its score between 0 and 1
	 */
	public static void compare(List<String> words){
		
		for(int i=0; i<words.size()-1; i++){
			for(int j=i+1; j<words.size(); j++){
				double distance = compute(words.get(i), words.get(j));
				System.out.println(words.get(i) +" -  " +  words.get(j) + " = " + distance);
			}
		}
	}

	
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	
	static void sparqlTest(){
		String fileName="C://Users//Gledys P//Desktop//Benchmark//SWS-TC-1.1//Ontology//Concepts.owl";
		FileManager.get().addLocatorClassLoader(Main.class.getClassLoader());
		Model model=FileManager.get().loadModel(fileName);
		
		String queryString=
				"PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> "+				
				"PREFIX owl: <http://www.w3.org/2002/07/owl#> "+
				"SELECT ?x WHERE { "+
				"?x owl:Class rdf:ID ?y "+
				"}"
			;
		Query query=QueryFactory.create(queryString);
		QueryExecution qexec=QueryExecutionFactory.create(query,model);
		try{
			ResultSet results=qexec.execSelect();
			while(results.hasNext()){
				QuerySolution soln=results.nextSolution();
				Literal name=soln.getLiteral("x");
				System.out.println(name);
			}
		}finally{
			qexec.close();
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<String> inputsServiceA=new ArrayList<String>();
		//inputsServiceA=null;
		List<String> inputsServiceB=new ArrayList<String>();
		//inputsServiceB=null;
		
		
		
		String filePath1="C://Users//Gledys P//Desktop//Benchmark//SWS-TC-1.1//Services//BookPriceInStore.owl";
		String filePath2="C://Users//Gledys P//Desktop//Benchmark//SWS-TC-1.1//Services//BookPrice.owl";
		
		OwlsService service1=new OwlsService();
		OwlsService service2=new OwlsService();
		
		System.out.println(service1.getServiceId());
		OwlsParser parser = new OwlsParser();
		//service1=null;
		//service2=null;
		
		service1=parser.parse(filePath1).get(0);
		service2=parser.parse(filePath2).get(0);
		
		//System.out.println(parser.parse(filePath1).get(0).getInputs().get(0).getId());
		//System.out.println(service1.getInputs().get(0).getId());

		//Get Inputs from service A
		System.out.println(service1.getInputs().size());
		System.out.println(service1.getInputs().size());
		
		for (int i = 0; i < service1.getInputs().size(); i++) {
			inputsServiceA.add(service1.getInputs().get(i).getId().toString());
			
		}
		
		for (int i = 0; i < service2.getInputs().size(); i++) {
			inputsServiceB.add(service2.getInputs().get(i).getId().toString());
			
			
		}
		
		//System.out.println(inputsServiceA.get(0).toString());
		//System.out.println(inputsServiceB.get(0).toString());
		compare(inputsServiceA);
		compare(inputsServiceB);
		
//		List<String> inputsServices=new ArrayList<String>();
//		inputsServices.addAll(inputsServiceA);
//		inputsServices.addAll(inputsServiceB);
//		compare(inputsServices);
//		
		compute(inputsServiceA.get(0),inputsServiceB.get(0));
		compute(inputsServiceA.get(1),inputsServiceB.get(0));
		
		for (int i = 0; i < inputsServiceA.size(); i++) {
			for (int j = 0; j < inputsServiceB.size(); j++) {
				compute(inputsServiceA.get(i),inputsServiceB.get(j));
			
			}
		}
		
		
//		String[] words = {"automobile", "car", "filter", "remove", "check", "find", "collect", "create","talk","speak"};
				//sparqlTest();
	//	compare(words);
				
				String fileName="C://Users//Gledys P//Desktop//Benchmark//SWS-TC-1.1//Ontology//Concepts.owl";
				//FileManager.get().addLocatorClassLoader(Main.class.getClassLoader());
				Model model=FileManager.get().loadModel(fileName);
				StmtIterator iter=model.listStatements();
				ResIterator resIter=model.listSubjectsWithProperty(model.getProperty("http://127.0.0.1/ontology/Concepts.owl#Phrase"));
				//System.out.println(resIter.toList().get(0).getURI());
				System.out.println(iter.toSet().size());
				for (int i = 0; i <iter.toSet().size(); i++) {
					Statement st=iter.toList().get(0);
					System.out.println(st.getSubject().getURI());
					System.out.println(st.getPredicate().getURI());
					System.out.println(st.getObject().toString());
					iter.next();
				};
				Resource res1 = model.getResource("http://127.0.0.1/ontology/Concepts.owl#Street");
				System.out.println(res1.getNameSpace());
//
//
//				Resource res2=(Resource)res1.getProperty();
//		
//				while (iter.hasNext()) {
//					//iter.next();
//					Integer i=0;
//					//iter.next();
//					Statement st=iter.toList().get(0);
//					//iter.close();
//					System.out.println(st.getSubject().getURI());
//					System.out.println(st.getPredicate().getURI());
//					System.out.println(st.getObject().toString());
//					System.out.println(i);
//					;
//				}
				//	model.write(System.out,"RDF/XML");
				//System.out.println(model.getProperty());
				
				
//		//JenaSearcher jenaSearcher=new JenaSearcher();
	//	jenaSearcher.compare(words);
		
//		for(int i=0; i<words.length-1; i++){
//			for(int j=i+1; j<words.length; j++){
//				double distance = compute(words[i], words[j]);
//				System.out.println(words[i] +" -  " +  words[j] + " = " + distance);
//			}
//		}
		
		//Test of Inputs, Outputs
//		String filePath="C://Users//Jorge//Desktop//Benchmark//SWS-TC-1.1//Services//AirportWeatherCheck.owl";
//		OwlsParser parser = new OwlsParser();
//		
//		System.out.println(parser.parse(filePath).get(0).getServiceName());
//		System.out.println(parser.parse(filePath).get(0).getInputs().get(0).getId());
		
	}

}
