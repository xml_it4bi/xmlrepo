/*
 * 
 */
package xml.wordnet.searcher;

import edu.cmu.lti.lexical_db.ILexicalDatabase;
import edu.cmu.lti.lexical_db.NictWordNet;
import edu.cmu.lti.ws4j.impl.WuPalmer;
import edu.cmu.lti.ws4j.util.WS4JConfiguration;
import xml.owlparser.parser.OwlsParser;

// TODO: Auto-generated Javadoc
/**
 * The Class WNSearcher.
 */
public class WNSearcher {

	/** The db. */
	private static ILexicalDatabase db = new NictWordNet();
	
	/**
	 * Compute.
	 *
	 * @param word1 the word1
	 * @param word2 the word2
	 * @return the double
	 */
	/*
	//available options of metrics
	private static RelatednessCalculator[] rcs = { new HirstStOnge(db),
			new LeacockChodorow(db), new Lesk(db), new WuPalmer(db),
			new Resnik(db), new JiangConrath(db), new Lin(db), new Path(db) };
	*/
	public static double compute(String word1, String word2) {
		WS4JConfiguration.getInstance().setMFS(true);
		double s = new WuPalmer(db).calcRelatednessOfWords(word1, word2);
		return s;
	}
	
	/**
	 * Compare.
	 *
	 * @param List of words
	 * This method compares two words and prints its score between 0 and 1
	 */
	public static void compare(String[] words){
		for(int i=0; i<words.length-1; i++){
			for(int j=i+1; j<words.length; j++){
				double distance = compute(words[i], words[j]);
				System.out.println(words[i] +" -  " +  words[j] + " = " + distance);
			}
		}
	}
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String[] words = {"automobile", "car", "filter", "remove", "check", "find", "collect", "create","talk","speak"};
		
		compare(words);
		
//		for(int i=0; i<words.length-1; i++){
//			for(int j=i+1; j<words.length; j++){
//				double distance = compute(words[i], words[j]);
//				System.out.println(words[i] +" -  " +  words[j] + " = " + distance);
//			}
//		}
		
		//Test of Inputs, Outputs
//		String filePath="C://Users//Jorge//Desktop//Benchmark//SWS-TC-1.1//Services//AirportWeatherCheck.owl";
//		OwlsParser parser = new OwlsParser();
//		
//		System.out.println(parser.parse(filePath).get(0).getServiceName());
//		System.out.println(parser.parse(filePath).get(0).getInputs().get(0).getId());
		
	}

}
