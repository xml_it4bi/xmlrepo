package xml.wordnet.searcher;

import java.util.HashSet;
import java.util.Set;

import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntModelSpec;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.util.FileManager;

public class WebServiceSimilarityDriver {
	
	/***********************************/
    /* Constants                       */
    /***********************************/

    // where the ontology should be
	static String SOURCE_URL = "http://www.semanticweb.org/ontologies/2013/3/Ontology1365003423152.owl";

    // where we've stashed it on disk for the time being
    static String SOURCE_FILE = "C://Users//Gledys P//Desktop//Benchmark//SWS-TC-1.1//Ontology//Concepts.owl";

    // the namespace of the ontology
    static String NS = SOURCE_URL + "#";

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
	

	    /***********************************/
	    /* External signature methods      */
	    /***********************************/
		
		OntModel m = ModelFactory.createOntologyModel( OntModelSpec.OWL_MEM );
		//Model m = ModelFactory.createDefaultModel();
		loadModel( m );
		Set<String> test = getConceptsNames(m, "AirportCode");
		for (String s : test){
			System.out.println(s);
		}
		
		

	}
	
	protected static void loadModel( OntModel m ) {
        FileManager.get().getLocationMapper().addAltEntry( SOURCE_URL, SOURCE_FILE );
        Model baseOntology = FileManager.get().loadModel( SOURCE_URL );
        m.addSubModel( baseOntology );

        // for compactness, add a prefix declaration st: (for Sam Thomas)
        m.setNsPrefix( "st", NS );
    }
	
	
	public static Set<String> getConceptsNames(Model model, String uri) {
        Set<String> conceptsName = new HashSet<String>();
        StmtIterator sIter = model.listStatements();
        for (; sIter.hasNext();) {
            Statement stmt = sIter.nextStatement();
            String subject = stmt.getSubject().toString();
            String predicat = stmt.getPredicate().toString();
            String object = stmt.getObject().toString();
            if (subject.contains(uri) || predicat.contains(uri) || object.contains(uri)) {
                String s = subject.substring(subject.indexOf("#") + 1);
                String p = predicat.substring(predicat.indexOf("#") + 1);
                String o = object.substring(object.indexOf("#") + 1);
                if ("subClassOf".equals(p) && !s.toLowerCase().equals("thing") && !o.toLowerCase().equals("thing")
                        && !s.contains("/") && !o.contains("/")
                        && !s.contains(":") && !o.contains(":")
                        && !s.contains("-") && !o.contains("-")) {
                    conceptsName.add(s);
                    conceptsName.add(o);
                }
            }
        }
        sIter.close();
        return conceptsName;
    }


}
