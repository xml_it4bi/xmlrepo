package xml.pddlparser.gui;

import java.io.File;

import javax.swing.filechooser.FileFilter;

/** 
 * 
 * This {@link SaveFile} class saves parse content into a file 
 *
 */
public class SaveFile extends FileFilter{
	private final String extension;
	private final String description;
	
	public SaveFile(String extension, String description){
		this.extension = extension;
		this.description = description;		
	}

	@Override
	public boolean accept(File file) {
		if (file.isDirectory()){
			return true;
		}
		return file.getName().endsWith(extension);
	}

	@Override
	public String getDescription() {
		return description + String.format(" (*%s)", extension);
		
	}
}

