package xml.pddlparser.gui;

import java.util.Scanner;
import javax.swing.JFileChooser;
import java.io.File; 


/**
 * 
 * {@link OpenFile} can get the file that it will be parsed
 *
 */

public class OpenFile {

	JFileChooser fileChooser = new JFileChooser();
	StringBuilder sb ;
	File file;
	StringBuilder pddlString;

	/**
	 * 
	 * This method chooses the file that it will be parsed
	 */
	public void PickMe() throws Exception{
		// reset attribute everytime open a new file
		this.sb = new StringBuilder();
		this.file = null;
		if(fileChooser.showOpenDialog(null)== JFileChooser.APPROVE_OPTION){
			//get the file
			file = fileChooser.getSelectedFile();
			
			//create a scanner for the file
			Scanner input = new Scanner(file);
			//FileWriter fw = new FileWriter(file.getPath());
			//read text from file
			while(input.hasNext()){
				sb.append(input.nextLine());
				sb.append("\n");
			}
			input.close();
		}
		else{
			sb.append("No file was selected");
		}
	}
		
	/**
	 * @return the file
	 */
	public File getFile() {
		return file;
	}


	/**
	 * @return the pddlString
	 */
	public StringBuilder getPddlString() {
		return pddlString;
	}

	/**
	 * @param pddlString the pddlString to set
	 */
	public void setPddlString(StringBuilder pddlString) {
		this.pddlString = pddlString;
	}

}
	



	


