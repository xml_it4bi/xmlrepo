package xml.pddlparser.gui;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
//import javax.swing.JTextField;
//import java.awt.Panel;
import java.awt.Image;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.UIManager;

import xml.owlparser.parser.OwlsParser;
import xml.owlparser.parser.PddlTransformer;
import xml.pddlparser.model.OwlsService;
import xml.pddlparser.model.PddlAction;


/**
 * 
 * {@link MainFrame} makes an interface in order to get the file and then visualize the result. 
 *
 */
public class MainFrame {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainFrame window = new MainFrame();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainFrame() {
		initialize();
	}

	/**
	 *  Initialize the contents of the frame with two TextAtrea, the left textArea show the Original document owl and the right part shows the result
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBackground(new Color(240, 240, 240));
		frame.setBounds(0, 0, 799, 505);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		//textArea_1.setBounds(529, 188, 450, 430);
		//frame.getContentPane().add(textArea_1);
		
		final JTextArea textArea_1 = new JTextArea();
		textArea_1.setBounds(212, 299, 335, 125);
		frame.getContentPane().add(textArea_1);
		
		final OpenFile of = new OpenFile();
		
		JButton btnGetTheFile = new JButton("Get the service 1");
		Image img = new ImageIcon(this.getClass().getResource("/img/open-file-icon.png")).getImage();
		btnGetTheFile.setIcon(new ImageIcon(img));
		btnGetTheFile.setFont(UIManager.getFont("CheckBox.font"));
		btnGetTheFile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				
				try{
					of.PickMe();
				}catch(Exception e){
					e.printStackTrace();
				}
				
				
				// If no file is selected, use default string "No file is selected"
				if(of.getFile() == null){
					textArea_1.setText("No file was selected");
				}
				else{
					
					}
					//of.setPddlString(); 
					textArea_1.setText(of.getPddlString().toString());
				}
				
			
		});
			
		btnGetTheFile.setBounds(116, 202, 202, 42);
		frame.getContentPane().add(btnGetTheFile);
		
		JButton btnGetTheService = new JButton("Get the service 2");
		btnGetTheService.setFont(UIManager.getFont("CheckBox.font"));
		btnGetTheService.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				
				try{
					of.PickMe();
				}catch(Exception e){
					e.printStackTrace();
				}
				
				
				// If no file is selected, use default string "No file is selected"
				if(of.getFile() == null){
					textArea_1.setText("No file was selected");
				}
				else{
					
					}
					//of.setPddlString(); 
					textArea_1.setText(of.getPddlString().toString());
				}
				
			
		});
		
		
		
		
		btnGetTheService.setBounds(413, 202, 202, 42);
		frame.getContentPane().add(btnGetTheService);
		
		
		
		
		
		
		
		
		Image img4 = new ImageIcon(this.getClass().getResource("/img/Save-icon.png")).getImage();
		
		JTextPane txtpnOriginalDocument = new JTextPane();
		txtpnOriginalDocument.setFont(new Font("Trebuchet MS", Font.BOLD, 13));
		txtpnOriginalDocument.setBackground(SystemColor.control);
		txtpnOriginalDocument.setText("Service 1");
		txtpnOriginalDocument.setBounds(163, 157, 155, 20);
		frame.getContentPane().add(txtpnOriginalDocument);
		
		JTextPane txtpnPddlDocument = new JTextPane();
		txtpnPddlDocument.setFont(new Font("Trebuchet MS", Font.BOLD, 13));
		txtpnPddlDocument.setText("Service 2");
		txtpnPddlDocument.setBackground(SystemColor.menu);
		txtpnPddlDocument.setBounds(480, 157, 135, 20);
		frame.getContentPane().add(txtpnPddlDocument);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setForeground(Color.BLUE);
		Image img2 = new ImageIcon(this.getClass().getResource("/img/logo1.png")).getImage();
		lblNewLabel.setIcon(new ImageIcon(img2));
		lblNewLabel.setBounds(32, 11, 186, 134);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("");
		Image img5 = new ImageIcon(this.getClass().getResource("/img/logo_it4bi2.png")).getImage();
		lblNewLabel_1.setIcon(new ImageIcon(img5));
		lblNewLabel_1.setBounds(609, 21, 119, 125);
		frame.getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("");
		Image img6 = new ImageIcon(this.getClass().getResource("/img/Drawing-1.png")).getImage();
		lblNewLabel_2.setIcon(new ImageIcon(img6));
		lblNewLabel_2.setBounds(287, 31, 312, 61);
		frame.getContentPane().add(lblNewLabel_2);
		
		
		
		
		
		
		
		
	}
}
