package xml.pddlparser.model;

/**
 * {@link PddlEffect} is used for storing effects in PDDL document
 *
 */
public class PddlEffect {
	private String datatype;
	private String name;
	
	public PddlEffect(){
		
	}
	
	public PddlEffect(String datatype, String name){
		this.datatype = datatype;
		this.name = name;
	}
	
	/**
	 * @return the datatype
	 */
	public String getDatatype() {
		return datatype;
	}
	/**
	 * @param datatype the datatype to set
	 */
	public void setDatatype(String datatype) {
		this.datatype = datatype;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	public String toString(){
		// remove character "_" in name
		int index = this.name.indexOf('_');
		if(index != -1){
			return this.datatype + " ?" + this.name.substring(index+1);			
		}
		return this.datatype + " ?" + this.name;
	}
}
