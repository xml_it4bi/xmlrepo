package xml.pddlparser.model;

/**
 * {@link PddlPrecondition} is used for storing precondition in PDDL document
 *
 */
public class PddlPrecondition {
	private String datatype;
	private String name;
	
	public PddlPrecondition(){
		
	}
	
	public PddlPrecondition(String datatype, String name){
		this.datatype = datatype;
		this.name = name;
	}
	
	/**
	 * @return the datatype
	 */
	public String getDatatype() {
		return datatype;
	}
	/**
	 * @param datatype the datatype to set
	 */
	public void setDatatype(String datatype) {
		this.datatype = datatype;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Print {@link PddlPrecondition} in format <b>(DATA_TYPE ?PRECONDITION)</b>
	 */
	public String toString(){
		// remove character "_" in name
		int index = this.name.indexOf('_');
		if(index != -1){
			return this.datatype + " ?" + this.name.substring(index+1);			
		}
		return this.datatype + " ?" + this.name;
	}
}
