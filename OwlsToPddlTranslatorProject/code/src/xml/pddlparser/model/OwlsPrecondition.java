package xml.pddlparser.model;

import java.util.LinkedList;
import java.util.List;

/**
 * {@link OwlsPrecondition} is used for storing precondition in OWLS document
 * <p>
 * Example document:
 * <profile:hasPrecondition	rdf:resource="#_HASCARD"/>	
 * ...
 * <expr:SWRL-Condition rdf:ID="_HASCARD" >
 * <rdfs:label>hasCard(_User, _CreditCardAmount)</rdfs:label>	
 * ...
 * <swrl:propertyPredicate.../>
 * <swrl:argument ../>
 * </p>
 *
 */
public class OwlsPrecondition {
	private String resource;
	private String id;
	private String label;
	private String propertyPredicate;
	private List<String> arguments = new LinkedList<>();
	
	public OwlsPrecondition(){
		
	}
	
	public OwlsPrecondition(String resource, String id, String label, String propertyPredicate, List<String> arguments){
		this.resource = resource;
		this.id = id;
		this.label = label;
		this.propertyPredicate = propertyPredicate;
		this.arguments = arguments;
	}
	
	
	/**
	 * @return the resource
	 */
	public String getResource() {
		return resource;
	}
	/**
	 * @param resource the resource to set
	 */
	public void setResource(String resource) {
		this.resource = resource;
	}
	
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}
	/**
	 * @param label the label to set
	 */
	public void setLabel(String label) {
		this.label = label;
	}
	/**
	 * @return the propertyPredicate
	 */
	public String getPropertyPredicate() {
		return propertyPredicate;
	}
	/**
	 * @param propertyPredicate the propertyPredicate to set
	 */
	public void setPropertyPredicate(String propertyPredicate) {
		this.propertyPredicate = propertyPredicate;
	}
	/**
	 * @return the arguments
	 */
	public List<String> getArguments() {
		return arguments;
	}
	/**
	 * @param argumentList the arguments to set
	 */
	public void setArguments(List<String> arguments) {
		this.arguments = arguments;
	}
	
	
}
