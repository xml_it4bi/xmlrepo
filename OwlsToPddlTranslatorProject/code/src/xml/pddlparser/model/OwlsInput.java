package xml.pddlparser.model;

/**
 * {@link OwlsInput} is used for storing information of input in OWLS document
 * <p>
 * Example document:
 * <profile:hasInput rdf:resource="#_TITLE"/>
 * <process:Input	rdf:ID="_TITLE">	
 *    <process:parameterType	
 *      rdf:datatype="http://www.w3.org/2001/XMLSchema#anyURI">http://127.0.0.1/ontology/books.owl#Title</process:parameterType>
 *    <rdfs:label></rdfs:label>	
 * </process:Input>
 * </p>
 */
public class OwlsInput {
	private String id;
	private String resource;
	private String dataType;
	private String label;
	
	public OwlsInput(){
		
	}
	
	public OwlsInput(String id, String resource, String dataType, String label){
		this.id = id;
		this.resource = resource;
		this.dataType = dataType;
		this.label = label;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the resource
	 */
	public String getResource() {
		return resource;
	}

	/**
	 * @param resource the resource to set
	 */
	public void setResource(String resource) {
		this.resource = resource;
	}

	/**
	 * @return the dataType
	 */
	public String getDataType() {
		return dataType;
	}

	/**
	 * @param dataType the dataType to set
	 */
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * @param label the label to set
	 */
	public void setLabel(String label) {
		this.label = label;
	}
	
	
}
