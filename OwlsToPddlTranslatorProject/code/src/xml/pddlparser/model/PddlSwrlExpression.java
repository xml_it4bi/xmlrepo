package xml.pddlparser.model;

import java.util.LinkedList;
import java.util.List;

import xml.owlparser.parser.PddlTransformer;

public class PddlSwrlExpression {
	private String id;
	private String propertyPredicate;
	private List<String> arguments = new LinkedList<>();
	
	public PddlSwrlExpression(){
		
	}
	
	public PddlSwrlExpression(String id, String propertyPredicate, List<String> arguments){
		this.id = id;
		this.propertyPredicate = propertyPredicate;
		this.arguments = arguments;
	}
	
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the propertyPredicate
	 */
	public String getPropertyPredicate() {
		return propertyPredicate;
	}
	/**
	 * @param propertyPredicate the label to set
	 */
	public void setPropertyPredicate(String propertyPredicate) {
		this.propertyPredicate = propertyPredicate;
	}
	/**
	 * @return the arguments
	 */
	public List<String> getArguments() {
		return arguments;
	}
	/**
	 * @param arguments the arguments to set
	 */
	public void setArguments(List<String> arguments) {
		this.arguments = arguments;
	}
	
	/**
	 * Print {@link PddlSwrlExpression} in format <b>(PROPERTY_PREDICATE DATA_TYPE1 ?ARGUMENT1 DATA_TYPE2 ARGUMENT2 ...)</b>
	 */
	public String toString(){
		// from argument: http://127.0.0.1/ontology/modified_books.owl#User
		// extract to get dataTypeRef: ontology/modified_books.owl#User
		// and get User
		List<String> extractedObjs = new LinkedList<>();
		List<String> dataTypeRefs = new LinkedList<>();
		for (String argument : this.getArguments()) {			
			// find position of "#"
			int index = argument.indexOf('#');
			String dataTypeRef = "";
			String entityName = "";
			if(index != -1){
				entityName = argument.substring(index+1);
				dataTypeRef = PddlTransformer.removeHostPrefix(argument);
				if(dataTypeRef.equals(arguments)){
					dataTypeRef = "";
				}
				else
				{
					dataTypeRef = dataTypeRef + " ";
				}
				extractedObjs.add(entityName);
				dataTypeRefs.add(dataTypeRef);
			}
		}
//		// Remove "_" for id
//		int index = this.getId().indexOf('_');
//		String newId = "";
//		String result = "";
//		if(index != -1){
//			newId = this.getId().substring(index+1);
//		}
//		else{
//			newId = this.getId();
//		}
		
		// Use property predicate to print
		int index = this.getPropertyPredicate().indexOf('#');
		String newPropertyPredicate = "";
		String result = "";
		if(index != -1){
			newPropertyPredicate = this.getPropertyPredicate().substring(index+1);
		}
		else{
			newPropertyPredicate = this.getPropertyPredicate();
		}

		for(int i =0; i< extractedObjs.size(); i++){
			result = result + dataTypeRefs.get(i) + "?" + extractedObjs.get(i) + " "; 
		}
		result = newPropertyPredicate + " " + result.trim();
		return result;
	}
}
