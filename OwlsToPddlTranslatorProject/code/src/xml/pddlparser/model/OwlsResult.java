package xml.pddlparser.model;

import java.util.LinkedList;
import java.util.List;

/**
 * {@link OwlsResult} is used for storing result in OWLS document 
 * <p>
 * Example document:
 * <profile:hasResult	rdf:resource="#_DELIVERY"/>	
 * ...
 * <process:Result rdf:ID="_DELIVERY">
 * 		<process:hasEffect>
 * 			<expr:SWRL-Expression>
 * 				<rdfs:comment>DELIVERY</rdfs:comment>
 * 			...
 * 			<swrl:propertyPredicate.../>
 * 			<swrl:argument ../>
 * </p>
 *
 */
public class OwlsResult {
	private String resource;
	private String id;
	private String comment;
	private String propertyPredicate;
	private List<String> arguments = new LinkedList<>();
	
	public OwlsResult(){
		
	}
	
	public OwlsResult(String resouce, String id, String comment, String propertyPredicate, List<String> arguments){
		this.resource = resouce;
		this.id = id;
		this.comment = comment;
		this.propertyPredicate = propertyPredicate;
		this.arguments = arguments;
	}
	
	/**
	 * @return the resource
	 */
	public String getResource() {
		return resource;
	}
	/**
	 * @param resource the resource to set
	 */
	public void setResource(String resource) {
		this.resource = resource;
	}
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}
	/**
	 * @param comment the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}
	/**
	 * @return the propertyPredicate
	 */
	public String getPropertyPredicate() {
		return propertyPredicate;
	}
	/**
	 * @param propertyPredicate the propertyPredicate to set
	 */
	public void setPropertyPredicate(String propertyPredicate) {
		this.propertyPredicate = propertyPredicate;
	}
	/**
	 * @return the arguments
	 */
	public List<String> getArguments() {
		return arguments;
	}
	/**
	 * @param arguments the argumentList to set
	 */
	public void setArguments(List<String> arguments) {
		this.arguments = arguments;
	}
	
	

}
