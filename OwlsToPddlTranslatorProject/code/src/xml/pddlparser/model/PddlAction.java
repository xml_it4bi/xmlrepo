package xml.pddlparser.model;

import java.util.LinkedList;
import java.util.List;

/**
 * {@link PddlAction} is used for holding PDDL document information
 * <p><b>Note:</b> One {@link PddlAction} will be correspondent to one {@link OwlsService}
 * </p>
 *
 */
public class PddlAction {
	private String actionName;
	private List<PddlPrecondition> preconditions = new LinkedList<>();
	private List<PddlEffect> effects = new LinkedList<>();
	private List<String> parameters = new LinkedList<>();
	private List<PddlSwrlExpression> owlsPrecondtions = new LinkedList<>();
	private List<PddlSwrlExpression> owlsResults = new LinkedList<>();
	
	/**
	 * @return the actionName
	 */
	public String getActionName() {
		return actionName;
	}
	/**
	 * @param actionName the actionName to set
	 */
	public void setActionName(String actionName) {
		this.actionName = actionName;
	}
	/**
	 * @return the preconditions
	 */
	public List<PddlPrecondition> getPreconditions() {
		return preconditions;
	}
	/**
	 * @param preconditions the preconditions to set
	 */
	public void setPreconditions(List<PddlPrecondition> preconditions) {
		this.preconditions = preconditions;
	}
	/**
	 * @return the parameters
	 */
	public List<String> getParameters() {
		return parameters;
	}
	/**
	 * @param parameters the parameters to set
	 */
	public void setParameters(List<String> parameters) {
		this.parameters = parameters;
	}
	/**
	 * @return the effects
	 */
	public List<PddlEffect> getEffects() {
		return effects;
	}
	/**
	 * @param effects the effects to set
	 */
	public void setEffects(List<PddlEffect> effects) {
		this.effects = effects;
	}
	
	
	
	/**
	 * @return the owlsPrecondtions
	 */
	public List<PddlSwrlExpression> getOwlsPrecondtions() {
		return owlsPrecondtions;
	}
	/**
	 * @param owlsPrecondtions the owlsPrecondtions to set
	 */
	public void setOwlsPrecondtions(List<PddlSwrlExpression> owlsPrecondtions) {
		this.owlsPrecondtions = owlsPrecondtions;
	}
	/**
	 * @return the owlsResults
	 */
	public List<PddlSwrlExpression> getOwlsResults() {
		return owlsResults;
	}
	/**
	 * @param owlsResults the owlsResults to set
	 */
	public void setOwlsResults(List<PddlSwrlExpression> owlsResults) {
		this.owlsResults = owlsResults;
	}
	
	/**
	 * Print {@link PddlAction} as simple format
	 */
	public String toString() {
		String result = "(action " + this.getActionName() + "\n";
		
		// process parameters 
		String parametersStr = "";
		for (String parameter : this.getParameters()) {
			int index = parameter.indexOf('_');
			if(index != -1){
				parametersStr = parametersStr + "?" + parameter.substring(index+1) + " ";			
			}
			else{				
				parametersStr = parametersStr + "?" + parameter + " ";
			}
		}
		// remove trailing space " "
		parametersStr = parametersStr.trim();
		if(parametersStr != ""){			
			parametersStr = ":parameters (" + parametersStr + ")";
			result = result + parametersStr + "\n";
		}
 		
		// process precondition
		String preconditionsStr = "";
		for (PddlPrecondition precondition : this.getPreconditions()) {
			preconditionsStr = preconditionsStr + "\t(" + precondition.toString() + ")\n";
		}
		
		for (PddlSwrlExpression expression: this.getOwlsPrecondtions()){
			preconditionsStr = preconditionsStr + "\t(" + expression.toString() + ")\n";
		}
		
		if(preconditionsStr != ""){
			// if only one element, we do not need "and"
			if(this.getPreconditions().size() + this.getOwlsPrecondtions().size() == 1){
				preconditionsStr = ":precondition\n" + preconditionsStr;			
				result = result + preconditionsStr + "\n";
			}
			else{
				preconditionsStr = ":precondition\n" + "(and \n" + preconditionsStr + ")";			
				result = result + preconditionsStr + "\n";
			}
		}
		
		// process effects 
		String effectsStr = "";
		for (PddlEffect effect : this.getEffects()) {
			effectsStr = effectsStr + "\t(" + effect.toString() + ")\n";
		}
		
		for (PddlSwrlExpression expression : this.getOwlsResults()) {
			effectsStr = effectsStr + "\t(" + expression.toString() + ")\n";
		}
		
		if(effectsStr != ""){	
			// if only one element, we do not need "and"
			if(this.getEffects().size() + this.getOwlsResults().size() ==1 ){
				effectsStr = ":effect\n" + effectsStr;
				result = result + effectsStr + "\n";
			}
			else{				
				effectsStr = ":effect\n" + "(and \n" + effectsStr + ")";
				result = result + effectsStr + "\n";
			}
		}
		result = result + ")\n";
		return result;
	}
}
