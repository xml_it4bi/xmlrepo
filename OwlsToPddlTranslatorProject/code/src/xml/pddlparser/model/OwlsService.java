package xml.pddlparser.model;

import java.util.LinkedList;
import java.util.List;

/**
 * {@link OwlsService} is used for holding OWL-S document after parsing
 *
 */
public class OwlsService {
	private String serviceId;
	private String serviceName;
	private List<OwlsInput> inputs = new LinkedList<>();
	private List<OwlsOutput> outputs = new LinkedList<>();
	private List<OwlsPrecondition> preconditions = new LinkedList<>();
	private List<OwlsResult> results = new LinkedList<>();
	private List<String> parameters = new LinkedList<>();
	
	public OwlsService(){
		
	}
	
	public OwlsService(String serviceId, String serviceName, List<String> parameters, List<OwlsInput> inputs, List<OwlsOutput> outputs, List<OwlsPrecondition> preconditions, List<OwlsResult> results){
		this.serviceId = serviceId;
		this.serviceName = serviceName;
		this.parameters = parameters;
		this.inputs = inputs;
		this.outputs = outputs;
		this.preconditions = preconditions;
		this.results = results;
	}
	
	
	
	
	/**
	 * @return the serviceName
	 */
	public String getServiceName() {
		return serviceName;
	}

	/**
	 * @param serviceName the serviceName to set
	 */
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	/**
	 * @return the serviceId
	 */
	public String getServiceId() {
		return serviceId;
	}

	/**
	 * @param serviceId the serviceId to set
	 */
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
	

	/**
	 * @return the parameters
	 */
	public List<String> getParameters() {
		return parameters;
	}

	/**
	 * @param parameters the parameters to set
	 */
	public void setParameters(List<String> parameters) {
		this.parameters = parameters;
	}


	/**
	 * @return the inputs
	 */
	public List<OwlsInput> getInputs() {
		return inputs;
	}
	/**
	 * @param inputs the inputs to set
	 */
	public void setInputs(List<OwlsInput> inputs) {
		this.inputs = inputs;
	}
	/**
	 * @return the outputs
	 */
	public List<OwlsOutput> getOutputs() {
		return outputs;
	}
	/**
	 * @param outputs the outputs to set
	 */
	public void setOutputs(List<OwlsOutput> outputs) {
		this.outputs = outputs;
	}
	/**
	 * @return the preconditions
	 */
	public List<OwlsPrecondition> getPreconditions() {
		return preconditions;
	}
	/**
	 * @param preconditions the preconditions to set
	 */
	public void setPreconditions(List<OwlsPrecondition> preconditions) {
		this.preconditions = preconditions;
	}
	/**
	 * @return the results
	 */
	public List<OwlsResult> getResults() {
		return results;
	}
	/**
	 * @param results the results to set
	 */
	public void setResults(List<OwlsResult> results) {
		this.results = results;
	}
	
	
}
