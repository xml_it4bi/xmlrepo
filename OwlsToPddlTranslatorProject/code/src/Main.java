import java.util.Scanner;

import xml.owlparser.parser.OwlsParser;
import xml.owlparser.parser.OwlsUtil;
import xml.pddlparser.model.OwlsService;

public class Main {
	public static void main(String[] args){
//		String dirPath = "C:\\Users\\Hien\\Desktop\\Thao\\Benchmark\\SWS-TC-1.1\\Services\\";
//		String filePath1 = dirPath +  "AddressBook.owl";
//		String filePath2 = dirPath +  "ABCHeadlines.owl";
//		
//		OwlsParser parser = new OwlsParser();
//		OwlsService service1 = parser.parse(filePath1).get(0);
//		OwlsService service2 = parser.parse(filePath2).get(0);
//		System.out.println(OwlsUtil.compareService(service1, service2));
		
//		System.out.println(WNSearcher.compute("Person", "Man"));
		
		//-------------- FOR REAL RUN WITH CONSOLE INTERFACE -------------------
		OwlsParser parser = new OwlsParser();
		Scanner scanInput = new Scanner(System.in);
		System.out.println("-------------------- WELCOME TO COMPARING SERVICE PROGRAM -------------------- ");
		int loop = 1;
		while(loop == 1){
			System.out.println("Please enter your first file path: ");
			String filePath1 = scanInput.nextLine();
			System.out.println("Please enter your second file path: ");
			String filePath2 = scanInput.nextLine();
			OwlsService service1 = parser.parse(filePath1).get(0);
			OwlsService service2 = parser.parse(filePath2).get(0);
			Integer result = OwlsUtil.compareService(service1, service2);
			
			switch (result) {
			case 1:
				System.out.println("Service 1 can be substituted by Service 2");
				break;
			case 2: 
				System.out.println("Service 1 can be composed with Service 2 by putting outputs of service 1 into inputs of service 2");
				break;
			case 3: 
				System.out.println("Service 2 can be composed with Service 1 by putting outputs of service 2 into inputs of service 1");

			default:
				System.out.println("Service 1 is different from service 2");
				break;
			}
			System.out.println("Please press 1 if you want to continue!");
			String loopStr = scanInput.nextLine();
			System.out.println(loopStr);
			loop = loopStr.equals("1") ? 1 :0;
		}
		System.out.println("Thank you for using our program ^_^");
	}
}
