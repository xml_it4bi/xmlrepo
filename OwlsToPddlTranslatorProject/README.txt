----------------------------------------------------------------------------------
------- WELCOME TO OWLS TO PDDL TRANSLATOR PROJECT --------
----------------------------------------------------------------------------------
Course: XML and Web Technology Project

Students:
+ Jorge Galicia
+ Ha Thao
+ Tzu-Jou Hsiao
+ Gledys Sulbaran

The project is orgranized as followed:
+ code: java source code of parser program
+ jar: the compile jar file of program
+ java-doc: java documentation of class and methods in program
+ report: Word document of report
